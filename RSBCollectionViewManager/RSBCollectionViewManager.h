//
//  RSBCollectionViewManager.h
//  RSBCollectionViewManager
//
//  Created by Damien on 22/06/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RSBCollectionViewManager.
FOUNDATION_EXPORT double RSBCollectionViewManagerVersionNumber;

//! Project version string for RSBCollectionViewManager.
FOUNDATION_EXPORT const unsigned char RSBCollectionViewManagerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RSBCollectionViewManager/PublicHeader.h>


