//
//  RSBCollectionViewManager.swift
//  RSBCollectionViewManager
//
//  Created by Damien on 22/06/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

//import UIKit

typealias RSBCollectionViewManagerCompletionHandler = (Bool) -> Void

public class RSBCollectionViewManager: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    weak private(set) var collectionView: UICollectionView?
    weak var scrollDelegate: UIScrollViewDelegate?
    
    private var _sectionItems = [RSBCollectionViewSectionItemProtocol]()
    var sectionItems : [RSBCollectionViewSectionItemProtocol] {
        set {
            _sectionItems = newValue
            for sectionItem in _sectionItems {
                self.registerSectionItem(sectionItem)
            }
            collectionView!.reloadData();
        }
        get {
            return _sectionItems
        }
    }
    
    init(collectionView: UICollectionView) {
        super.init()
        self.collectionView = collectionView
        self.collectionView!.delegate = self
        self.collectionView!.dataSource = self
    }

        // MARK: - Public Methods
        // MARK: Cell Items
    
    func removeCellItems(cellItems: [RSBCollectionViewCellItemProtocol],
                         inout fromSectionItem sectionItem: RSBCollectionViewSectionItemProtocol,
                                               completion: RSBCollectionViewManagerCompletionHandler?) {
        let section = sectionItems.indexOf({$0 === sectionItem})!
        var indexPaths = [NSIndexPath]()
        let indexes = NSMutableIndexSet()
        
        for cellItem in cellItems {
            let row  = sectionItem.cellItems!.indexOf({$0 === cellItem})
            precondition(row != nil, "It's impossible to remove cell items that are not contained in section item")
            
            indexPaths.append(NSIndexPath(forRow: row!, inSection: section))
            indexes.addIndex(row!)
        }
        sectionItem.cellItems!.removeElementsAtIndexes(indexes)
        self.collectionView?.performBatchUpdates({ 
            self.collectionView?.deleteItemsAtIndexPaths(indexPaths)
            }, completion: { (finished) in
                completion?(finished)
        })
    }

    func insertCellItems(cellItems: [RSBCollectionViewCellItemProtocol],
                         inout toSectionItem sectionItem: RSBCollectionViewSectionItemProtocol,
                                             atIndexes indexes: NSIndexSet,
                                                       completion: RSBCollectionViewManagerCompletionHandler?) {
        precondition(indexes.firstIndex <= sectionItem.cellItems!.count, "It's impossible to insert item at index that is larger than count of cell items in this section")
        for cellItem in cellItems {
            cellItem.dynamicType.registerCellForCollectionView(collectionView!)
        }
        guard let section = sectionItems.indexOf({$0 === sectionItem}) else {
            return
        }
        
        sectionItem.cellItems!.insertElements(cellItems, atIndexes: indexes)
        
        var indexPaths = [NSIndexPath]()
        for idx in indexes {
            indexPaths.append(NSIndexPath(forRow: idx, inSection: section))
        }
        
        collectionView?.performBatchUpdates({
            self.collectionView?.insertItemsAtIndexPaths(indexPaths)
            }, completion: { (finished) in
                completion?(finished)
        })
    }
    
    func replaceCellItemsAtIndexes(indexes: NSIndexSet,
                                   withCellItems cellItems: [RSBCollectionViewCellItemProtocol],
                                                 inout inSectionItem sectionItem: RSBCollectionViewSectionItemProtocol,
                                                                     completion: RSBCollectionViewManagerCompletionHandler?) {
        precondition(indexes.count == cellItems.count, "It's impossible to replace not equal count of cell items")
        for cellItem in cellItems {
            cellItem.dynamicType.registerCellForCollectionView(collectionView!)
        }
        
        sectionItem.cellItems!.replaceRange(indexes.firstIndex ..< indexes.lastIndex, with: cellItems)
        guard let section = sectionItems.indexOf({$0 === sectionItem}) else {
            return
        }
        var indexPaths = [NSIndexPath]()
        indexes.enumerateIndexesUsingBlock { (index, stop) in
            indexPaths.append(NSIndexPath(forRow: index, inSection: section))
        }
        
        collectionView?.performBatchUpdates({
            self.collectionView?.reloadItemsAtIndexPaths(indexPaths)
            }, completion: { (finished) in
                completion?(finished)
        })
    }
    
        // MARK: Section Items

    func removeSectionItems(sectionItems: [RSBCollectionViewSectionItemProtocol], completion: RSBCollectionViewManagerCompletionHandler?) {
        let indexes = NSMutableIndexSet()
        for sectionItem in sectionItems {
            let section = self.sectionItems.indexOf({$0 === sectionItem})
            precondition(section != nil, "It's impossible to remove section items that are not contained in section items array")
            indexes.addIndex(section!)
        }

        self.sectionItems.removeElementsAtIndexes(indexes)
        collectionView?.deleteSections(indexes)
        
        collectionView?.performBatchUpdates({ 
            self.collectionView?.deleteSections(indexes)
            }, completion: { (finished) in
                completion?(finished)
        })
    }

    func insertSectionItems(sectionItems: [RSBCollectionViewSectionItemProtocol],
                            atIndexes indexes: NSIndexSet,
                                      completion: RSBCollectionViewManagerCompletionHandler?) {
        precondition(indexes.firstIndex <= self.sectionItems.count, "It's impossible to insert item at index that is larger than count of section items")
        for sectionItem in sectionItems {
            registerSectionItem(sectionItem)
        }
        
        self.sectionItems.insertElements(sectionItems, atIndexes: indexes)
        
        collectionView?.performBatchUpdates({ 
            self.collectionView?.insertSections(indexes)
            }, completion: { (finished) in
                completion?(finished)
        })
    }

    func replaceSectionItemsAtIndexes(indexes: NSIndexSet,
                                      withSectionItems sectionItems: [RSBCollectionViewSectionItemProtocol],
                                                       completion: RSBCollectionViewManagerCompletionHandler?) {
        precondition(indexes.count == sectionItems.count, "It's impossible to replace not equal count of section items")
        for sectionItem in sectionItems {
            registerSectionItem(sectionItem)
        }
        
        self.sectionItems.replaceRange(indexes.firstIndex ..< indexes.lastIndex, with: sectionItems)
            
        collectionView?.performBatchUpdates({ 
            self.collectionView?.reloadSections(indexes)
            }, completion: { (finished) in
                completion?(finished)
        })
    }

        // MARK: Others
    
    func setSectionItems(sectionItems: [RSBCollectionViewSectionItemProtocol], completion: RSBCollectionViewManagerCompletionHandler?) {
        _sectionItems = sectionItems
        for sectionItem in _sectionItems {
            self.registerSectionItem(sectionItem)
        }
        if completion != nil {
            collectionView?.performBatchUpdates({
                self.collectionView?.reloadSections(NSIndexSet.init(indexesInRange: NSMakeRange(0, self.sectionItems.count)))
                }, completion: { (finished) in
                    completion?(finished)
            })
        } else {
            collectionView!.reloadData();
        }
    }

    func frameForCellItem(cellItem: RSBCollectionViewCellItemProtocol, inSectionItem sectionItem: RSBCollectionViewSectionItemProtocol) -> CGRect? {
        guard let sectionItemIndex = sectionItems.indexOf({$0 === sectionItem}) else {
            return nil
        }
        guard let cellItemIndex = sectionItem.cellItems!.indexOf({$0 === cellItem}) else {
            return nil
        }
        let indexPath = NSIndexPath(forRow: cellItemIndex, inSection: sectionItemIndex)
        return collectionView?.layoutAttributesForItemAtIndexPath(indexPath)?.frame
    }
    
    func scrollToCellItem(cellItem: RSBCollectionViewCellItemProtocol,
                          inSectionItem sectionItem: RSBCollectionViewSectionItemProtocol,
                                        atScrollPosition scrollPosition: UICollectionViewScrollPosition, animated: Bool) {
        guard let sectionItemIndex = sectionItems.indexOf({$0 === sectionItem}) else {
            return
        }
        guard let cellItemIndex = sectionItem.cellItems!.indexOf({$0 === cellItem}) else {
            return
        }
        let indexPath = NSIndexPath(forItem: cellItemIndex, inSection: sectionItemIndex)
        collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: scrollPosition, animated: animated)
    }

    func scrollToTopAnimated(animated: Bool) {
        let sectionItem = sectionItems.first
        scrollToCellItem((sectionItem?.cellItems?.first)!, inSectionItem: sectionItem!, atScrollPosition: UICollectionViewScrollPosition.Top, animated: animated)
    }
    
        // MARK: Helpers
    
    private func registerSectionItem(sectionItem: RSBCollectionViewSectionItemProtocol) {
        for cellItem in sectionItem.cellItems! {
            cellItem.dynamicType.registerCellForCollectionView(collectionView!)
        }
        for (key, reusableView) in sectionItem.reusableViewItems! {
            reusableView.dynamicType.registerReusableViewForCollectionView(collectionView!, kind: key)
        }
    }
    
        // MARK: UICollectionViewDataSource
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return sectionItems.count
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sectionItem = sectionItems[section]
        if sectionItem.cellItems == nil {
            return 0
        }
        return sectionItem.cellItems!.count
    }
    
    public func collectionView(collectionView: UICollectionView,
                               viewForSupplementaryElementOfKind kind: String,
                                                                 atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let sectionItem = sectionItems[indexPath.section]
        if let reusableViewItem = sectionItem.reusableViewItems?[kind] {
            return reusableViewItem.reusableViewForCollectionView(collectionView, kind: kind, indexPath: indexPath)
        }
        return UICollectionReusableView()
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let sectionItem = sectionItems[indexPath.section]
        let cellItem = sectionItem.cellItems![indexPath.item]
        let cell = cellItem.cellForCollectionView(collectionView, indexPath: indexPath)
        cellItem.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
}
