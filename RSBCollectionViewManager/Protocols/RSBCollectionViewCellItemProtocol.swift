//
//  RSBCollectionViewCellItemProtocol.swift
//  RSBCollectionViewManager
//
//  Created by Damien on 22/06/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation

extension RSBCollectionViewCellItemProtocol {
    func itemDidSelectInCollectionView(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath) {}
    func itemShouldSelectInCollectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath) -> Bool { return true }
    func itemShouldHighlightInCollectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath) -> Bool { return true }
    
    func willDisplayCell(cell: UICollectionViewCell, forItemAtIndexPath: NSIndexPath, inCollectionView collectionView: UICollectionView) {}
    func configureCell(cell: UICollectionViewCell, atIndexPath: NSIndexPath) {}
}

public protocol RSBCollectionViewCellItemProtocol: AnyObject {
    func sizeForCollectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, indexPath: NSIndexPath) -> CGSize
    func cellForCollectionView(collectionView: UICollectionView, indexPath: NSIndexPath) -> UICollectionViewCell
    
    static func registerCellForCollectionView(collectionView : UICollectionView)
    
    func configureCell(cell: UICollectionViewCell, atIndexPath: NSIndexPath)
    func itemDidSelectInCollectionView(collectionView: UICollectionView, atIndexPath indexPath: NSIndexPath)
    func itemShouldSelectInCollectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath) -> Bool
    func itemShouldHighlightInCollectionView(collectionView: UICollectionView, atIndexPath: NSIndexPath) -> Bool
    func willDisplayCell(cell: UICollectionViewCell, forItemAtIndexPath: NSIndexPath, inCollectionView collectionView: UICollectionView)
}