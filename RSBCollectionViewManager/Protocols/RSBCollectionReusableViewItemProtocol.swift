//
//  RSBCollectionViewReusableViewItemProtocol.swift
//  RSBCollectionViewManager
//
//  Created by Damien on 22/06/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation

public protocol RSBCollectionReusableViewItemProtocol: AnyObject {
    func sizeForCollectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, section: Int, kind: String) -> CGSize
    func reusableViewForCollectionView(collectionView: UICollectionView, kind: String, indexPath: NSIndexPath) -> UICollectionReusableView
    static func registerReusableViewForCollectionView(collectionView: UICollectionView, kind: String)
}