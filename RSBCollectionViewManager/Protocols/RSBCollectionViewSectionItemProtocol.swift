//
//  RSBCollectionViewSectionItemProtocol.swift
//  RSBCollectionViewManager
//
//  Created by Damien on 22/06/16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

import Foundation

public protocol RSBCollectionViewSectionItemProtocol: AnyObject {
    var cellItems : [RSBCollectionViewCellItemProtocol]? { get set }
    var reusableViewItems : [String: RSBCollectionReusableViewItemProtocol]? { get set }
}
